import json
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from calculateScore import calculateScore
# the goal of this code is to take a json file with info about a game and use
# it to generate a match report filled with the relevant stats

# look at the score and return the name of the person who won
def findWinner(score, p1Name, p2Name):
    return p1Name

def createH2Hbar(title,percentage,leftNum,rightNum):
    fig, ax = plt.subplots()

    start = 0
    middle = leftNum
    if percentage:
        end = 100
    else:
        end = leftNum + rightNum

    ax.broken_barh([(start, middle), (middle,end)], [10, 9], facecolors=('#6259D8', '#E53F08'))
    ax.set_ylim(5, 15)
    ax.set_xlim(0, 100)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_yticks([15, 25])
    ax.set_xticks([0, 25, 50, 75, 100])

    ax.set_axisbelow(True) 

    ax.set_yticklabels(['Q1'])
    ax.grid(axis='x')
    if percentage:
        ax.text(middle-6, 14.5, str(leftNum)+"%", fontsize=8)
        ax.text((end)-6, 14.5, str(rightNum)+"%", fontsize=8)
    else:
        ax.text(middle-6, 14.5, str(leftNum), fontsize=8)
        ax.text((end)-6, 14.5, str(rightNum), fontsize=8)

    fig.suptitle('Title', fontsize=16)

    leg1 = mpatches.Patch(color='#6259D8', label='Player 1')
    leg2 = mpatches.Patch(color='#E53F08', label='Player 2')
    ax.legend(handles=[leg1, leg2], ncol=3)

    plt.show()

def genMatchReport(values):

    score, name = calculateScore(values)

    p1name = 'tom'
    p2name = 'laragh'
    winnerName = findWinner(score, p1name, p2name)
    
    print('The winner is: ', winnerName)
    print('The score was\nPlayer 1: ', score[0], '\nPlayer 2: ', score[1])

    createH2Hbar('Test', True, 48, 52)

def main():
    with open("MatchExample.json", "r") as read_file:
        values = json.load(read_file)
    genMatchReport(values)

# if run as a script, use the MatchExample.json as an example
if __name__ == "__main__":
    # execute only if run as a script
    main()