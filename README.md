# Table Tennis Stat Collector

This project aims to provide a user-friendly interface for table tennis players 
to record statistics from their matches when doing video analysis. Then present 
a match report based on the statistics that easily digestible.

Note that this project is still a work in progress.

## Requirements
This program was developed using python 3.8.3. This program needs a number of 
python packages to run correctly, they are:  
PySimpleGUI

I use anaconda to manage python packages. The command for using conda to 
install pysimple gui is (run in anaconda prompt window):  
`conda install -c conda-forge pysimplegui`  