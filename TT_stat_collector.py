import PySimpleGUI as sg
import json
import datetime
import string
import calculateScore

def validateDate(date_text):
    try:
        datetime.datetime.strptime(date_text, '%d/%m/%Y')
    except ValueError:
        return False
    return True

def validateTime(date_text):
    try:
        datetime.datetime.strptime(date_text, '%H:%M')
    except ValueError:
        return False
    return True

def format_filename(s):
    """Take a string and return a valid filename constructed from the string.
    Uses a whitelist approach: any characters not present in valid_chars are
    removed. Also spaces are replaced with underscores.
    
    Note: this method may produce invalid filenames such as ``, `.` or `..`
    When I use this method I prepend a date string like '2009_01_15_19_46_32_'
    and append a file extension like '.txt', so I avoid the potential of using
    an invalid filename.
    """
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ','_') # I don't like spaces in filenames.
    return filename
    
hand = ["Right", "Left"]
styles = ["Attacker", "Defender"]

sg.theme('DarkAmber')	# Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text("Match Information", size=(30, 1), justification='center', font=("Helvetica", 16))],
            [sg.Text('Match Title*:'), sg.InputText(key='-MT-'), sg.Text(size=(16,1), key='-MTErr-')],
            [sg.Text('Location:'), sg.InputText(key='-L-')],
            [sg.Text('Event:'), sg.InputText(key='-E-')],
            [sg.Text('Stage:'), sg.InputText(key='-S-')],
            [
                sg.Text("Best of (maximum number of sets):"),
                sg.Slider((1,7), key='_bestOf_', orientation='h', enable_events=True, disable_number_display=True,
                resolution=2, default_value=5),  
                sg.T('5', key='_RIGHT_')
            ],
            [sg.Text('URL to Video:'), sg.InputText(key='-URL-')],
            [sg.Text('Time (format 24h hh:mm):'), sg.InputText(key='-T-'), sg.Text(size=(16,1), key='-TErr-')],
            [sg.Text('Date (format dd/mm/yyyy):'), sg.InputText(key='-D-'), sg.Text(size=(16,1), key='-DErr-')],
            [sg.Text("Player 1 Info",size=(30, 1), justification='center', font=("Helvetica", 13))],
            [sg.Text('Player 1 Name*:'), sg.InputText(key='-P1N-'), sg.Text(size=(16,1), key='-P1NErr-')],
            [sg.Text('Player 1 Hand*:'),sg.Listbox(hand, size=(15, len(hand)), key='-HAND1-')],
            [sg.Text('Player 1 Style:'), sg.Listbox(styles, size=(15,len(styles)), key='-STYLES1-')],
            [sg.Text("Player 2 Info",size=(30, 1), justification='center', font=("Helvetica", 13))],
            [sg.Text('Player 2 Name*:'), sg.InputText(key='-P2N-'), sg.Text(size=(16,1), key='-P2NErr-')],
            [sg.Text('Player 2 Hand*:'),sg.Listbox(hand, size=(15, len(hand)), key='-HAND2-')],
            [sg.Text('Player 2 Style:'), sg.Listbox(styles, size=(15,len(styles)), key='-STYLES2-')],
            [sg.Button('Ok'), sg.Button('Cancel')] ]

# Create the Window
window = sg.Window('Match Details', layout)

takePoints = False

# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancel':	# if user closes window or clicks cancel
        break
    elif event == 'Ok':
        problem = False
        # check time and date format
        if values['-D-'] and not validateDate(values['-D-']):
            #print(values['-D-'])
            window['-DErr-'].update("**Date Format Incorrect**")  # show error message
            problem = True
        else:
            window['-DErr-'].update("")  # remove error message
        if values['-T-'] and not validateTime(values['-T-']):
            window['-TErr-'].update("**Time Format Incorrect**")  # show error message
            problem = True
        else:
            window['-TErr-'].update("")  # remove error message
        if not values['-MT-']:
            window['-MTErr-'].update("**Title Required**")  # show error message
            problem = True
        else:
            window['-MTErr-'].update("")  # remove error message
        if not values['-P1N-']:
            window['-P1NErr-'].update("**Name Required**")  # show error message
            problem = True
        else:
            window['-P1NErr-'].update("")  # remove error message
        if not values['-P2N-']:
            window['-P2NErr-'].update("**Name Required**")  # show error message
            problem = True
        else:
            window['-P2NErr-'].update("")  # remove error message
        if not problem:
            takePoints = True
            break
        
    window['_RIGHT_'].update(values['_bestOf_']-1)

window.close()

if takePoints:
    # edit the best of to be odd number
    values['_bestOf_'] = values['_bestOf_'] - 1
    # dump the match details to a json file
    filename = 'TTST_' + format_filename(values['-MT-']) + '.json'
    with open(filename, 'w') as json_file:
        json.dump(values, json_file, indent=4)

    pointn = 1
    setn = 1
    #ply1p = 0, ply1s = 0, ply2p = 0, ply2s = 0

    try:
        while True:
            ply1p, ply1s, ply2p, ply2s, setn = calculateScore(values)

            layout = [
                [sg.Text('Score: ')],
                [sg.Text('Point '+ pointn + ' Set ' + setn)],
                [sg.Text('Server: ')],
                [sg.Text('Number of Successful Shots in rally: ')],
                [sg.Text('Last successful shot type: ')],
                [sg.Text('Last successful shot type: ')],
                [sg.Text('Last successful shot placement (long back hand, ...): ')],
                [sg.Text('Type of point end (winner, unforced error, neither): ')],
                # if unforced error
                [sg.Text('Miss shot type (bh/fh): ')],
                [sg.Text('Miss shot type: ')],
                [sg.Text('Miss shot placement (net, long, wide)')]
                [sg.Button('Ok'), sg.Button('Cancel')] ]
            while True:
                event, values = window.read()
                if event == sg.WIN_CLOSED or event == 'Cancel':	# if user closes window or clicks cancel
                    break

            pointn = pointn+1
    except TypeError:
        print("Error. No points have been input")


    



