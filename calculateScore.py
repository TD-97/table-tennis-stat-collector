import json


# need to write this so that it changes the server
def changeServer(curServer, firstServer, firstReceiver):
    if curServer==firstServer:
        curServer=firstReceiver
    else:
        curServer=firstServer
    return curServer


# uses the points (11-5, 14-12 returns true, 8-4, 6-6 returns false) 
def checkSetOver(p1p, p2p):
    if (p1p<11 and p2p<11) or abs(p2p-p1p)<2:
        return False
    else:
        return True


# takes the dictionary values, and calculates the current score and server based on that dictionary
def calculateScore(values):
    # set the current set and find the first server
    # dummy values while this is wip
    score = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]
    server = 'Player 1'
    try:
        curSet = 1
        firstServerP = 0
        firstReceiverP = 0
        firstServer = values['-points-']['-point1-']['-server-']
        if firstServer == 'Player 1':
            firstReceiver = 'Player 2'
        else:
            firstReceiver = 'Player 1'

        # get the points from the dictionary
        points = values['-points-']
        curServer = firstServer # set first server
        duece = False
        setOver = False

        # loop through each of the points
        for point in points:
            # print(point)
            curServer = values['-points-'][point]['-server-']
            # print(curServer)
            # if the first server is serving
            if curServer == firstServer:
                # if an even number of shots, first receiver wins point
                if (points[point]['-shotN-'] % 2) == 0:
                    firstReceiverP += 1
                else:
                    firstServerP += 1
            # if the first receiver is serving
            else:
                if (points[point]['-shotN-'] % 2) == 0:
                    firstServerP += 1
                else:
                    firstReceiverP += 1

            # check if duece
            if firstServerP > 9 and firstReceiverP > 9:
                duece = True
            else:
                duece = False

            # check if a set is over
            setOver = checkSetOver(firstServerP, firstReceiverP)

            if not setOver:
                # check if the current server needs to be changed
                if (firstReceiverP+firstServerP)%2==0:
                    curServer=changeServer(curServer, firstServer, firstReceiver)
                elif duece==True:
                    curServer=changeServer(curServer, firstServer, firstReceiver)
            else:
                # update the score list
                if firstServer == 'Player 1':
                    score[0][curSet-1] = firstServerP
                    score[1][curSet-1] = firstReceiverP
                else:
                    score[0][curSet-1] = firstReceiverP
                    score[1][curSet-1] = firstServerP
                curSet += 1         # increment the sets
                firstServerP = 0    # reset the points
                firstReceiverP = 0

                # set the server for the new set
                if (curSet%2 == 0):
                    curServer = firstReceiver
                else:
                    curServer = firstServer

                continue

            # update the score list
            if firstServer=='Player 1':
                score[0][curSet-1] = firstServerP
                score[1][curSet-1] = firstReceiverP
            else:
                score[0][curSet-1] = firstReceiverP
                score[1][curSet-1] = firstServerP
        return score, curServer
    except KeyError:
        print("Error. No points in file.")
        return score, server


def main():
    with open("MatchExample.json", "r") as read_file:
        values = json.load(read_file)

    score, server = calculateScore(values)
    print('Score: ', score, '. Server: ', server)


# if run as a script, use the MatchExample.json as an example
if __name__ == "__main__":
    # execute only if run as a script
    main()