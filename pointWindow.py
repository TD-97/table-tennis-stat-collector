import PySimpleGUI as sg
import json
import datetime
import string
from calculateScore import calculateScore

def format_filename(s):
    """Take a string and return a valid filename constructed from the string.
    Uses a whitelist approach: any characters not present in valid_chars are
    removed. Also spaces are replaced with underscores.
    
    Note: this method may produce invalid filenames such as ``, `.` or `..`
    When I use this method I prepend a date string like '2009_01_15_19_46_32_'
    and append a file extension like '.txt', so I avoid the potential of using
    an invalid filename.
    """
    valid_chars = "-_() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ','_') # I don't like spaces in filenames.
    return filename

matchDetails = {
    "-matchTitle-": "Tom Vs. Sinead",
    "-location-": "National",
    "-event-": "Nationals",
    "-stage-": "Final",
    "-bestOf-": 8.0,
    "-URL-": "",
    "-time-": "15:00",
    "-date-": "3/6/2020",
    "-player1Name-": "Tom",
    "-player1Hand-": [
        "Left"
    ],
    "-player1Style-": [
        "Attacker"
    ],
    "-player2Name-": "Sinead",
    "-player2Hand-": [
        "Right"
    ],
    "-player2Style-": [
        "Attacker"
    ]
}

def getTile(setn, ply1p, ply2p):
    tile = [[sg.Text('Set '+str(setn), background_color='lightblue', justification='center', size=(5, 1))],
            [sg.Text(str(ply1p), background_color='lightblue', justification='center', size=(5, 1))], 
            [sg.Text(ply2p, background_color='lightblue', justification='center', size=(5, 1))]]
    return tile


# edit the best of to be odd number
matchDetails['-bestOf-'] = matchDetails['-bestOf-'] - 1
# dump the match details to a json file
filename = 'TTST_' + format_filename(matchDetails['-matchTitle-']) + '.json'
with open(filename, 'w') as json_file:
    json.dump(matchDetails, json_file, indent=4)

matchDetails['-points-'] = {}
storePoint = True

pointn = 1
setn = 1
ply1p = 0
ply1s = 0
ply2p = 0
ply2s = 0

shots = ["Loop (against topspin)", "Loop (against backspin)", "Counter", "Block", "Touch", "Push", 
"Chop", "Serve", "Flick", "Half-long", "Lob"]
sides = ["Forehand", "Backhand"]
placement =  ["Long Backhand", "Long Middle", "Long Forehand", "Short Backhand", "Short Middle", "Short Forehand"]
end = ["Winner", "Unforced Error", "None"]
missPlace = ["Net", "Wide", "Long"]

while storePoint:
    score, server = calculateScore(matchDetails)

    scoreboard = [sg.Text('Score: ')]
    # the scoreboard
    sb1 = [[sg.Text('', background_color='lightblue', justification='center', size=(10, 1))],
            [sg.Text('Player 1', background_color='lightblue', justification='center', size=(10, 1))], 
            [sg.Text('Player 2', background_color='lightblue', justification='center', size=(10, 1))]]
    scoreboard.append(sg.Column(sb1, background_color='lightblue'))
    for i in range(1,8):
        scoreboard.append(sg.Column(getTile(i,score[0][i-1],score[1][i-1]), background_color='lightblue'))
    
    layout = [
        scoreboard,
        [sg.Text('Point '+str(pointn)+' Set '+str(setn))],
        [sg.Text('Server: '), sg.Spin(values=('Player 1', 'Player 2'), initial_value=server, key='-server-')],
        [sg.Text('Number of Successful Shots in rally: '),sg.InputText(key='-shotN-'), sg.Text(size=(16,1), key='-shotNErr-')],
        [sg.Text('Last successful shot type: '), sg.Listbox(shots, size=(20, 3), key='-lastShotType-')],
        [sg.Text('Last successful shot type: '), sg.Listbox(sides, size=(15, len(sides)), key='-lastShotSide-')],
        [sg.Text('Last successful shot placement: '), sg.Listbox(placement, size=(15, 3), key='-lastShotPlacement-')],
        [sg.Text('Type of point end: '),sg.Listbox(end, size=(15, len(end)), key='-endReason-')],
        # if unforced error
        [sg.Text('Miss shot type (bh/fh): '), sg.Listbox(sides, size=(20, len(sides)), key='-missSide-')],
        [sg.Text('Miss shot type: '), sg.Listbox(shots, size=(15, 5), key='-missType-')],
        [sg.Text('Miss shot placement net long wide'), sg.Listbox(missPlace, size=(15, len(missPlace)), key='-missPlacement-')],
        [sg.Button('Record Point'), sg.Button('End Match')] ]
    
    # Create the Window
    window = sg.Window('Match Details', layout)
    
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'End Match':	# if user closes window or clicks cancel
            storePoint = False
            break
        elif event == 'Record Point':
            storePoint = True
            # make sure that the number of shots is entered correctly
            if not values['-shotN-'].isdigit() or not values["-shotN-"]:
                window['-shotNErr-'].update("Error")  # show error message
                storePoint = False
            else:
                values['-shotN-'] = int(values['-shotN-'])
                window['-shotNErr-'].update("")  # remove error message
                break

    print(storePoint)

    # store the point in the json file if store point is true
    if storePoint:
        values['-pointNumber-'] = pointn
        nestedDictKey = "-point"+str(pointn)+'-'
        matchDetails['-points-'][nestedDictKey] = values
        pointn = pointn+1
        print(matchDetails)

    window.close()
with open(filename, 'w') as json_file:
    json.dump(matchDetails, json_file, indent=4)